-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 02 Jun 2019 pada 22.06
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `puguh`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kapal`
--

CREATE TABLE `kapal` (
  `id_kapal` int(11) NOT NULL,
  `nama_kapal` varchar(115) NOT NULL,
  `suply_aliran_listrik` text NOT NULL,
  `pembersihan_lambung` text NOT NULL,
  `pengecatan_lambung` text NOT NULL,
  `kran_induk` text NOT NULL,
  `kran_pembuangan` text NOT NULL,
  `saringan_induk` text NOT NULL,
  `plat_dasar` text NOT NULL,
  `plat_lambung` text NOT NULL,
  `gading_dasar` text NOT NULL,
  `kemudi` text NOT NULL,
  `as_kemudi` text NOT NULL,
  `sistem_kemudi` text NOT NULL,
  `jangkar` text NOT NULL,
  `baling` text NOT NULL,
  `pembersihan_tangki` text NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `tanggal_keluar` date NOT NULL,
  `tahun` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kapal`
--

INSERT INTO `kapal` (`id_kapal`, `nama_kapal`, `suply_aliran_listrik`, `pembersihan_lambung`, `pengecatan_lambung`, `kran_induk`, `kran_pembuangan`, `saringan_induk`, `plat_dasar`, `plat_lambung`, `gading_dasar`, `kemudi`, `as_kemudi`, `sistem_kemudi`, `jangkar`, `baling`, `pembersihan_tangki`, `tanggal_masuk`, `tanggal_keluar`, `tahun`) VALUES
(1, 'asu', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', '2019-06-03', '2019-06-03', '2015'),
(2, 'b', 'b', 'b', '', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', '2019-06-05', '2019-06-29', '2019'),
(3, 'h', 'gu', 'h', '', 'h', 'j', 'h', 'u', 'y', 't', 'w', 'q', 'q', 'q', 'q', 'q', '2019-06-25', '2019-06-24', '9999'),
(4, 'KM. \" ADI NUSANTARA \"', 'Tanggal : 20 Agustus 2015 sampai dengan tgl. 27 Agustus 2015 Pemakaian : 15 Ampere / 220 Volt / 3 Phase. Pasang & lepas penyambungan kabel listrik', 'Tanggal : 20 Agustus 2015 sampai dengan tgl. 27 Agustus 2015 Pemakaian : 15 Ampere / 220 Volt / 3 Phase. Pasang & lepas penyambungan kabel listrik', 'Tanggal : 20 Agustus 2015 sampai dengan tgl. 27 Agustus 2015 Pemakaian : 15 Ampere / 220 Volt / 3 Phase. Pasang & lepas penyambungan kabel listrik', 'Tanggal : 20 Agustus 2015 sampai dengan tgl. 27 Agustus 2015 Pemakaian : 15 Ampere / 220 Volt / 3 Phase. Pasang & lepas penyambungan kabel listrik', 'Tanggal : 20 Agustus 2015 sampai dengan tgl. 27 Agustus 2015 Pemakaian : 15 Ampere / 220 Volt / 3 Phase. Pasang & lepas penyambungan kabel listrik', 'Tanggal : 20 Agustus 2015 sampai dengan tgl. 27 Agustus 2015 Pemakaian : 15 Ampere / 220 Volt / 3 Phase. Pasang & lepas penyambungan kabel listrik', 'Tanggal : 20 Agustus 2015 sampai dengan tgl. 27 Agustus 2015 Pemakaian : 15 Ampere / 220 Volt / 3 Phase. Pasang & lepas penyambungan kabel listrik', 'Tanggal : 20 Agustus 2015 sampai dengan tgl. 27 Agustus 2015 Pemakaian : 15 Ampere / 220 Volt / 3 Phase. Pasang & lepas penyambungan kabel listrik', 'Tanggal : 20 Agustus 2015 sampai dengan tgl. 27 Agustus 2015 Pemakaian : 15 Ampere / 220 Volt / 3 Phase. Pasang & lepas penyambungan kabel listrik', 'Tanggal : 20 Agustus 2015 sampai dengan tgl. 27 Agustus 2015 Pemakaian : 15 Ampere / 220 Volt / 3 Phase. Pasang & lepas penyambungan kabel listrik', 'Tanggal : 20 Agustus 2015 sampai dengan tgl. 27 Agustus 2015 Pemakaian : 15 Ampere / 220 Volt / 3 Phase. Pasang & lepas penyambungan kabel listrik', 'Tanggal : 20 Agustus 2015 sampai dengan tgl. 27 Agustus 2015 Pemakaian : 15 Ampere / 220 Volt / 3 Phase. Pasang & lepas penyambungan kabel listrik', 'Tanggal : 20 Agustus 2015 sampai dengan tgl. 27 Agustus 2015 Pemakaian : 15 Ampere / 220 Volt / 3 Phase. Pasang & lepas penyambungan kabel listrik', 'Tanggal : 20 Agustus 2015 sampai dengan tgl. 27 Agustus 2015 Pemakaian : 15 Ampere / 220 Volt / 3 Phase. Pasang & lepas penyambungan kabel listrik', 'Tanggal : 20 Agustus 2015 sampai dengan tgl. 27 Agustus 2015 Pemakaian : 15 Ampere / 220 Volt / 3 Phase. Pasang & lepas penyambungan kabel listrik', '2019-06-04', '2019-06-29', '2015');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `ktp` int(11) NOT NULL,
  `email` varchar(155) NOT NULL,
  `password` varchar(155) NOT NULL,
  `nama` varchar(155) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `kapal`
--
ALTER TABLE `kapal`
  ADD PRIMARY KEY (`id_kapal`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ktp`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kapal`
--
ALTER TABLE `kapal`
  MODIFY `id_kapal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `ktp` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
