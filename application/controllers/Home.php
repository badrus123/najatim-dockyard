<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Model_kapal');
		$this->load->helper('url');
		$this->load->library('form_validation');
	}
	public function index()
	{
		$data['kapal'] = $this->Model_kapal->get_data();
		// print_r($data);
		$this->load->view('Admin/home_admin',$data);
	}
	public function Detail_kapal($id_kapal)
	{
		$data['kapal'] = $this->Model_kapal->get_data_detail($id_kapal);
		// print_r($data);
		$this->load->view('Admin/detail_admin',$data);
	}
	public function delete_kapal($id_kapal){
		$table = 'kapal';
		$this->Model_kapal->delete_kapal($table,$id_kapal);
		$this->session->set_flashdata('alert', 'sukses_delete');
		redirect("Home/index");
	  }
	public function edit_kapal($id_kapal)
	{
		$data['kapal'] = $this->Model_kapal->get_data_detail($id_kapal);
		// print_r($data);
		$this->load->view('Admin/edit_admin',$data);
	}
}
