<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_user extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Model_kapal');
		$this->load->helper('url');
		$this->load->library('form_validation');
	}
	public function index()
	{
		$data['kapal'] = $this->Model_kapal->get_data();
		// print_r($data);
		$this->load->view('dashboard',$data);
	}
	public function table_user()
	{
		$data['kapal'] = $this->Model_kapal->get_data();
		// print_r($data);
		$this->load->view('tabel',$data);
	}
	public function Detail_kapal($id_kapal)
	{
		$data['kapal'] = $this->Model_kapal->get_data_detail($id_kapal);
		// print_r($data);
		$this->load->view('detail',$data);
	}
}
