<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Login_model');
		$this->load->helper('url');
		$this->load->library('form_validation');
	}
	public function index()
	{
		$this->load->view('login');
	}
	public function register()
	{
		$this->load->view('register');
	}
	public function add_user(){
    	$nama = $this->input->post('nama');
    	$email = $this->input->post('email');
    	$password = $this->input->post('password');


    	$data = array(
    		"nama"=>$nama,
            "email"=>$email,
            "password"=>md5($password),
			"level"=>"user",
    	);
    	$this->Login_model->register($data,'user');
    	
    	redirect('Login/index');
	}
	public function check(){
        $email = $this->input->post("email");
        $password = $this->input->post("password");
        $data = array(
			'email' => $email,
			'password' => md5($password)
			);
        $login = $this->Login_model->check($data);
        
        if ($login){	
                $data1 = array(
                    'logodin' => 'logodin',
                    'nama' => $login->nama,
                    'email' => $login->email,
                    'ktp' => $login->ktp,
                    'level' => $login->level
                );
                
                $this->session->set_userdata($data1);
                
                if( $this->session->userdata('level') == 'user'){
					redirect('Home_user/index');
					// echo($this->session->userdata('level'));
                }
                else if( $this->session->userdata('level') == 'admin'){
					redirect('Home/index');
					// echo($this->session->userdata('level'));
                }
                
                
            }else{
                $this->session->set_flashdata('message','Error Login');
                redirect('Login/index');
            }
    }
    
    public function logout(){
        $this->session->sess_destroy();
        redirect('Login/index');
    }
}
