<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_kapal extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Model_kapal');
		$this->load->helper('url');
		$this->load->library('form_validation');
	}
	public function index()
	{
		$this->load->view('Admin/form_kapal');
	}

	public function update_kapal($id_kapal){
		$nama_kapal = $this->input->post('nama_kapal');
    	$suply_aliran_listrik = $this->input->post('suply_aliran_listrik');
    	$pembersihan_lambung = $this->input->post('pembersihan_lambung');
		$kran_induk = $this->input->post('kran_induk');
		$kran_pembuangan = $this->input->post('kran_pembuangan');
		$saringan_induk = $this->input->post('saringan_induk');
		$plat_dasar = $this->input->post('plat_dasar');
		$plat_lambung = $this->input->post('plat_lambung');
		$gading_dasar = $this->input->post('gading_dasar');
		$kemudi = $this->input->post('kemudi');
		$as_kemudi = $this->input->post('as_kemudi');
		$sistem_kemudi = $this->input->post('sistem_kemudi');
		$jangkar = $this->input->post('jangkar');
		$baling = $this->input->post('baling');
		$pembersihan_tangki = $this->input->post('pembersihan_tangki');
		$tanggal_masuk = $this->input->post('tanggal_masuk');
		$tanggal_keluar = $this->input->post('tanggal_keluar');
		$tahun = $this->input->post('tahun');
		$table = 'kapal';

    	$data = array(
    		"nama_kapal"=>$nama_kapal,
            "suply_aliran_listrik"=>$suply_aliran_listrik,
            "pembersihan_lambung"=>$pembersihan_lambung,
			"kran_induk"=>$kran_induk,
			"kran_pembuangan"=>$kran_pembuangan,
			"saringan_induk"=>$saringan_induk,
			"plat_dasar"=>$plat_dasar,
			"plat_lambung"=>$plat_lambung,
			"gading_dasar"=>$gading_dasar,
			"kemudi"=>$kemudi,
			"as_kemudi"=>$as_kemudi,
			"sistem_kemudi"=>$sistem_kemudi,
			"jangkar"=>$jangkar,
			"baling"=>$baling,
			"pembersihan_tangki"=>$pembersihan_tangki,
			"tanggal_masuk"=>$tanggal_masuk,
			"tanggal_keluar"=>$tanggal_keluar,
			"tahun"=>$tahun
			
    	);
		$bisa = $this->Model_kapal->update_kapal($data,'kapal',$id_kapal);

		redirect('Home/index');


	  }

	public function input(){
    	$nama_kapal = $this->input->post('nama_kapal');
    	$suply_aliran_listrik = $this->input->post('suply_aliran_listrik');
    	$pembersihan_lambung = $this->input->post('pembersihan_lambung');
		$kran_induk = $this->input->post('kran_induk');
		$kran_pembuangan = $this->input->post('kran_pembuangan');
		$saringan_induk = $this->input->post('saringan_induk');
		$plat_dasar = $this->input->post('plat_dasar');
		$plat_lambung = $this->input->post('plat_lambung');
		$gading_dasar = $this->input->post('gading_dasar');
		$kemudi = $this->input->post('kemudi');
		$as_kemudi = $this->input->post('as_kemudi');
		$sistem_kemudi = $this->input->post('sistem_kemudi');
		$jangkar = $this->input->post('jangkar');
		$baling = $this->input->post('baling');
		$pembersihan_tangki = $this->input->post('pembersihan_tangki');
		$tanggal_masuk = $this->input->post('tanggal_masuk');
		$tanggal_keluar = $this->input->post('tanggal_keluar');
		$tahun = $this->input->post('tahun');

    	$data = array(
    		"nama_kapal"=>$nama_kapal,
            "suply_aliran_listrik"=>$suply_aliran_listrik,
            "pembersihan_lambung"=>$pembersihan_lambung,
			"kran_induk"=>$kran_induk,
			"kran_pembuangan"=>$kran_pembuangan,
			"saringan_induk"=>$saringan_induk,
			"plat_dasar"=>$plat_dasar,
			"plat_lambung"=>$plat_lambung,
			"gading_dasar"=>$gading_dasar,
			"kemudi"=>$kemudi,
			"as_kemudi"=>$as_kemudi,
			"sistem_kemudi"=>$sistem_kemudi,
			"jangkar"=>$jangkar,
			"baling"=>$baling,
			"pembersihan_tangki"=>$pembersihan_tangki,
			"tanggal_masuk"=>$tanggal_masuk,
			"tanggal_keluar"=>$tanggal_keluar,
			"tahun"=>$tahun
			
    	);
    	$this->Model_kapal->insert($data,'kapal');
    	
    	redirect('Home/index');
    }
}
