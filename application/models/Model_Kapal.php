<?php 
defined('BASEPATH') or exit('No direct script access allowed');
/**
* 
*/
class Model_kapal extends CI_Model
{    
	// private $table = "kapal";
	public function get_data()
	{
		$this->db->select('*');
        $this->db->from('kapal');
        $query = $this->db->get();
        return $query->result_array();
	}
	public function get_data_detail($id_kapal)
	{
		$this->db->select('*');
		$this->db->from('kapal');
		$this->db->where('id_kapal',$id_kapal);
        $query = $this->db->get();
        return $query->result_array();
	}
	public function insert($data,$table)
    {
        return $this->db->insert($table,$data);
	}
	public function update_kapal($data,$table,$id_kapal)
    {
        $this->db->where('id_kapal', $id_kapal);
        $update = $this->db->update($table,$data);
        if ($update){
          return TRUE;
        }else{
          return FALSE;
        }
	}
	public function delete_kapal($table,$id_kapal){
		$this->db->where('id_kapal', $id_kapal);
		$delete = $this->db->delete($table);
		if ($delete){
		  return TRUE;
		}else{
		  return FALSE;
		}
	  }
}
