<!doctype html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Learning Codeigniter</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="https://i.imgur.com/QRAUqs9.png">
    <link rel="shortcut icon" href="https://i.imgur.com/QRAUqs9.png">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url().'assets/assets/css/cs-skin-elastic.css'?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/assets/css/style.css'?>">
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet" />

   <style>
    #weatherWidget .currentDesc {
        color: #ffffff!important;
    }
        .traffic-chart {
            min-height: 335px;
        }
        #flotPie1  {
            height: 150px;
        }
        #flotPie1 td {
            padding:3px;
        }
        #flotPie1 table {
            top: 20px!important;
            right: -10px!important;
        }
        .chart-container {
            display: table;
            min-width: 270px ;
            text-align: left;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        #flotLine5  {
             height: 105px;
        }

        #flotBarChart {
            height: 150px;
        }
        #cellPaiChart{
            height: 160px;
        }

    </style>
</head>

<body>
    <!-- Left Panel -->
    <?php $this->load->view('layout');?>

    <!-- /#left-panel -->
    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <?php $this->load->view('layout_header');?>
        <!-- /#header -->
        <!-- Content -->
        <div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
                <!-- Widgets  -->
				<div class="col-lg-12 margin-bawah">
					<h4 class="mb-3">Tabel User</h4>
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Tabel Data Kapal</strong>
							</div>
							<?php foreach ($kapal as  $value) { ?> 
                            <div class="card-body">
							<div class="col-lg-6">
								<div>
									<h4>Tanggal masuk</h4>
									<p><?= $value['tanggal_masuk'] ?></p>
								</div>
															<div>
									<h4>Tanggal keluar</h4>
									<p><?= $value['tanggal_keluar'] ?></p>
								</div>
															<div>
									<h4>Tahun</h4>
									<p><?= $value['tahun'] ?></p>
								</div>
								<div>
									<h4>Nama Kapal</h4>
									<p><?= $value['nama_kapal'] ?></p>
								</div>
								<div>
									<h4>Suply Aliran listrik</h4>
									<p><?= $value['suply_aliran_listrik'] ?></p>
								</div>
								<div>
									<h4>Pembersihan Lambung</h4>
									<p><?= $value['pembersihan_lambung'] ?></p>
								</div>
								<div>
									<h4>Pengecatan lambung</h4>
									<p><?= $value['pengecatan_lambung'] ?></p>
								</div>
								<div>
									<h4>Kran induk</h4>
									<p><?= $value['kran_induk'] ?></p>
								</div>
								<div>
									<h4>Kran pembuangan</h4>
									<p><?= $value['kran_pembuangan'] ?></p>
								</div>
								<div>
									<h4>Saringan Induk</h4>
									<p><?= $value['saringan_induk'] ?></p>
								</div>
                       		</div>
							<div class="col-lg-6">
								<div>
									<h4>Plat Dasar</h4>
									<p><?= $value['plat_dasar'] ?></p>
								</div>
															<div>
									<h4>Plat lambung</h4>
									<p><?= $value['plat_lambung'] ?></p>
								</div>
															<div>
									<h4>Gading Dasar</h4>
									<p><?= $value['gading_dasar'] ?></p>
								</div>
															<div>
									<h4>Kemudi</h4>
									<p><?= $value['kemudi'] ?></p>
								</div>
															<div>
									<h4>as kemudi</h4>
									<p><?= $value['as_kemudi'] ?></p>
								</div>
															<div>
									<h4>Sistem Kemudi</h4>
									<p><?= $value['sistem_kemudi'] ?></p>
								</div>
															<div>
									<h4>Jangkar</h4>
									<p><?= $value['jangkar'] ?></p>
								</div>
															<div>
									<h4>Baling baling</h4>
									<p><?= $value['baling'] ?></p>
								</div>
															<div>
									<h4>Pembersihan Tangki</h4>
									<p><?= $value['pembersihan_tangki'] ?></p>
								</div>
							</div>
                        </div>
					</div>
					<?php } ?>
				</div>
				
            <!-- /#add-category -->
            </div>
            <!-- .animated -->
        </div>
        <!-- /.content -->
        <div class="clearfix"></div>

        <!-- /.site-footer -->
    </div>
    <!-- /#right-panel -->

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="<?php echo base_url().'assets/assets/js/main.js'?>"></script>

    <!--  Chart js -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.bundle.min.js"></script>

    <!--Chartist Chart-->
    <script src="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartist-plugin-legend@0.6.2/chartist-plugin-legend.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/jquery.flot@0.8.3/jquery.flot.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flot-pie@1.0.0/src/jquery.flot.pie.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flot-spline@0.0.1/js/jquery.flot.spline.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/simpleweather@3.1.0/jquery.simpleWeather.min.js"></script>
    <script src="assets/js/init/weather-init.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.js"></script>
    <script src="<?php echo base_url().'assets/assets/js/init/fullcalendar-init.js'?>"></script>


</body>
</html>
