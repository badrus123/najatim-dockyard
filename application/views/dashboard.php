<!doctype html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Learning Codeigniter</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="https://i.imgur.com/QRAUqs9.png">
    <link rel="shortcut icon" href="https://i.imgur.com/QRAUqs9.png">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url().'assets/assets/css/cs-skin-elastic.css'?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/assets/css/style.css'?>">
	
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet" />

   <style>
    #weatherWidget .currentDesc {
        color: #ffffff!important;
    }
        .traffic-chart {
            min-height: 335px;
        }
        #flotPie1  {
            height: 150px;
        }
        #flotPie1 td {
            padding:3px;
        }
        #flotPie1 table {
            top: 20px!important;
            right: -10px!important;
        }
        .chart-container {
            display: table;
            min-width: 270px ;
            text-align: left;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        #flotLine5  {
             height: 105px;
        }

        #flotBarChart {
            height: 150px;
        }
        #cellPaiChart{
            height: 160px;
        }
		table.sortable thead {
		background-color:#eee;
		color:#666666;
		font-weight: bold;
		cursor: default;
	}
    </style>
</head>

<body>
    <!-- Left Panel -->
    <?php $this->load->view('layout');?>

    <!-- /#left-panel -->
    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <?php $this->load->view('layout_header');?>
        <!-- /#header -->
        <!-- Content -->
        <div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
				<div class="col-lg-12 margin-bawah">
						<h4 class="mb-3">Dashboard</h4>
							<div class="card">
								<div class="card-header">
									<strong class="card-title">Chart Data Kapal</strong>
								</div>
								<div class="card-body">
									<div id="chartContainer" style="height: 370px; width: 100%;"></div>
								</div>
						</div>
					</div>
                <!-- Widgets  -->
				<div class="col-lg-12 margin-bawah">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Tabel Data Kapal</strong>
                            </div>
                            <div class="card-body">
							
                                <table class="table sortable">
									<thead>
										<tr>
											<th scope="col">Nama kapal</th>
											<th scope="col">Tanggal masuk</th>
											<th scope="col">Tanggal keluar</th>
											<th scope="col">Tahun</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($kapal as $nilai) {?>
											<tr>
												<td><a href="<?= site_url('Home_user/Detail_kapal/'.$nilai['id_kapal']); ?>"><?= $nilai['nama_kapal'] ?></a></td>
												<td><?= $nilai['tanggal_masuk'] ?></td>
												<td><?= $nilai['tanggal_keluar'] ?></td>
												<td><?= $nilai['tahun'] ?></td>
											</tr>
										<?php }?>
									</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <!-- /#add-category -->
            </div>
            <!-- .animated -->
        </div>
        <!-- /.content -->
        <div class="clearfix"></div>

        <!-- /.site-footer -->
    </div>
    <!-- /#right-panel -->

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="<?php echo base_url().'assets/assets/js/main.js'?>"></script>

    <!--  Chart js -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.bundle.min.js"></script>

    <!--Chartist Chart-->
    <script src="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartist-plugin-legend@0.6.2/chartist-plugin-legend.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/jquery.flot@0.8.3/jquery.flot.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flot-pie@1.0.0/src/jquery.flot.pie.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flot-spline@0.0.1/js/jquery.flot.spline.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/simpleweather@3.1.0/jquery.simpleWeather.min.js"></script>
    <script src="assets/js/init/weather-init.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.js"></script>
	<script src="<?php echo base_url().'assets/assets/js/init/fullcalendar-init.js'?>"></script>
	<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

	<script src="https://kryogenix.org/code/browser/sorttable/sorttable.js"></script>
	<script>
		window.onload = function () {

		var chart = new CanvasJS.Chart("chartContainer", {
			exportEnabled: true,
			animationEnabled: true,
			title:{
				text: "Chart Data Kapal"
			}, 
			axisX: {
				title: "States"
			},
			axisY: {
				title: "kapal Masuk - Units",
				titleFontColor: "#4F81BC",
				lineColor: "#4F81BC",
				labelFontColor: "#4F81BC",
				tickColor: "#4F81BC"
			},
			toolTip: {
				shared: true
			},
			legend: {
				cursor: "pointer",
				itemclick: toggleDataSeries
			},
			data: [{
				type: "column",
				name: "banyak Kapal",
				showInLegend: true,      
				yValueFormatString: "#,##0.# Units",
				dataPoints: [
					{ label: "2015",  y: 10 },
					{ label: "2016", y: 20 },
					{ label: "2017", y: 30 },
					{ label: "2018",  y: 40 },
					{ label: "2019",  y: 50 }
				]
			}]
		});
		chart.render();

		function toggleDataSeries(e) {
			if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
				e.dataSeries.visible = false;
			} else {
				e.dataSeries.visible = true;
			}
			e.chart.render();
		}

		}
	</script>

</body>
</html>
