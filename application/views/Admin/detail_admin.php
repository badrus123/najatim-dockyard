<?php 
    $this->load->view('Admin/nav_admin');
 ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url().'Home_admin/index'?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Detail Obat</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <!-- small box -->
 <!-- TABLE: LATEST ORDERS -->
 <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Kapal</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
           <?php foreach ($kapal as  $value) { ?> 
            <div class="box-body">
              <div class="table-responsive">
              	 <div class="col-md-6">
								 						<div>
                                <h4>Tanggal masuk</h4>
                                <p><?= $value['tanggal_masuk'] ?></p>
                            </div>
														<div>
                                <h4>Tanggal keluar</h4>
                                <p><?= $value['tanggal_keluar'] ?></p>
                            </div>
														<div>
                                <h4>Tahun</h4>
                                <p><?= $value['tahun'] ?></p>
                            </div>
                            <div>
                                <h4>Nama Kapal</h4>
                                <p><?= $value['nama_kapal'] ?></p>
                            </div>
                            <div>
                                <h4>Suply Aliran listrik</h4>
 								                <p><?= $value['suply_aliran_listrik'] ?></p>
                            </div>
                            <div>
                                <h4>Pembersihan Lambung</h4>
                                <p><?= $value['pembersihan_lambung'] ?></p>
                            </div>
                            <div>
                                <h4>Pengecatan lambung</h4>
                                <p><?= $value['pengecatan_lambung'] ?></p>
                            </div>
                            <div>
                                <h4>Kran induk</h4>
                                <p><?= $value['kran_induk'] ?></p>
                            </div>
                            <div>
                                <h4>Kran pembuangan</h4>
                                <p><?= $value['kran_pembuangan'] ?></p>
                            </div>
                            <div>
                                <h4>Saringan Induk</h4>
                                <p><?= $value['saringan_induk'] ?></p>
                            </div>
                        </div>
												<div class="col-md-6">
														<div>
                                <h4>Plat Dasar</h4>
                                <p><?= $value['plat_dasar'] ?></p>
                            </div>
														<div>
                                <h4>Plat lambung</h4>
                                <p><?= $value['plat_lambung'] ?></p>
                            </div>
														<div>
                                <h4>Gading Dasar</h4>
                                <p><?= $value['gading_dasar'] ?></p>
                            </div>
														<div>
                                <h4>Kemudi</h4>
                                <p><?= $value['kemudi'] ?></p>
                            </div>
														<div>
                                <h4>as kemudi</h4>
                                <p><?= $value['as_kemudi'] ?></p>
                            </div>
														<div>
                                <h4>Sistem Kemudi</h4>
                                <p><?= $value['sistem_kemudi'] ?></p>
                            </div>
														<div>
                                <h4>Jangkar</h4>
                                <p><?= $value['jangkar'] ?></p>
                            </div>
														<div>
                                <h4>Baling baling</h4>
                                <p><?= $value['baling'] ?></p>
                            </div>
														<div>
                                <h4>Pembersihan Tangki</h4>
                                <p><?= $value['pembersihan_tangki'] ?></p>
                            </div>
												</div>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <button  class="btn btn-sm btn-default btn-flat pull-right">print</button>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
         <?php } ?>
</div>

<?php 
 $this->load->view('Admin/footer_admin');
?>
