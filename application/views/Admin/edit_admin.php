<?php 
$this->load->view('Admin/nav_admin');

 ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Edit Dockyard kapal
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url().'Home_admin/index'?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url().'Form_obat/index'?>">Forms</a></li>
        <li class="active">Form Kapal</li>
      </ol>
    </section>

    <section class="content">
        <div class="box box-default">
            <div class="box-header with-border">
                    <h3 class="box-title">Input Data Kapal</h3>
                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
            </div>
				<!-- /.box-header -->
				<?php foreach ($kapal as  $value) { ?> 
                <div class="box-body">
                    <div class="row">
                        <form method="POST" action="<?php echo base_url('Form_kapal/update_kapal/'.$value['id_kapal']); ?>" enctype="multipart/form-data">
                        <div class="col-md-6">
                            <div>
                                <h4>Nama kapal</h4>
                                <input  class="form-control select2" type="text" name="nama_kapal" placeholder="nama kapal" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>Suply Aliran Listrik</h4>
                                <input  class="form-control select2" type="text" name="suply_aliran_listrik" placeholder="suply aliran listrik" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>Pembersihan Lambung</h4>
                                <input  class="form-control select2" type="text" name="pembersihan_lambung" placeholder="pembersihan lambung" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>Pengecatan Lambung</h4>
                                <input  class="form-control select2" type="text" name="pengecatan_lambung" placeholder="pengecatan lambung" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>kran induk</h4>
                                <input  class="form-control select2" type="text" name="kran_induk" placeholder="kran induk" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>kran pembuangan</h4>
                                <input  class="form-control select2" type="text" name="kran_pembuangan" placeholder="kran pembuangant" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>Saringan Induk</h4>
                                <input  class="form-control select2" type="text" name="saringan_induk" placeholder="saringan induk" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>Plat dasar</h4>
                                <input  class="form-control select2" type="text" name="plat_dasar" placeholder="plat dasar" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>Plat Lambung</h4>
                                <input  class="form-control select2" type="text" name="plat_lambung" placeholder="plat lambung" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>Gading dasar</h4>
                                <input  class="form-control select2" type="text" name="gading_dasar" placeholder="Gading dasar" required="required" style=" width: 100%;">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div>
                                <h4>kemudi</h4>
                                <input  class="form-control select2" type="text" name="kemudi" placeholder="kemudi" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>As kemudi</h4>
                                <input  class="form-control select2" type="text" name="as_kemudi" placeholder="As Kemudi" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>Sistem kemudi</h4>
                                <input  class="form-control select2" type="text" name="sistem_kemudi" placeholder="sistem keamanan" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>jangkar</h4>
                                <input  class="form-control select2" type="text" name="jangkar" placeholder="jangkar" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>Baling Baling</h4>
                                <input  class="form-control select2" type="text" name="baling" placeholder="Baling Baling" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>Pembersihan Tangki</h4>
                                <input  class="form-control select2" type="text" name="pembersihan_tangki" placeholder="Pembersihan Tangki" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>Tanggal masuk</h4>
                                <input  class="form-control select2" type="date" name="tanggal_masuk" placeholder="Tanggal Masuk" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>Tanggal keluar</h4>
                                <input  class="form-control select2" type="date" name="tanggal_keluar" placeholder="Tanggal Keluar" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                <h4>Tahun</h4>
                                <input  class="form-control select2" type="text" name="tahun" placeholder="Tahun" required="required" style=" width: 100%;">
                            </div>
                            <div>
                                </br>
                                </br>
                                <input class="form-control select2 btn btn-primary" type="submit" value="submit" >
                            </div>
                            </form>
                        </div>
					</div>
					<?php } ?>
                </div>
        </div>
    </section>




</div>
<?php 
 $this->load->view('Admin/footer_admin');
?>
