<?php 
    $this->load->view('Admin/nav_admin');
 ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
			Dashboard Dockyard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url().'Home_admin/index'?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard Dockyard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
 <!-- TABLE: LATEST ORDERS -->
 <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Repair List</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin sortable">
                  <thead>
                  <tr>
                    <th>Nama kapal</th>
                    <th>Tanggal masuk</th>
                    <th>Tanggal keluar</th>
										<th>Tahun</th>
										<th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
										<?php foreach ($kapal as $nilai) {?>
											<tr>
												<td><a href="<?= site_url('Home/Detail_kapal/'.$nilai['id_kapal']); ?>"><?= $nilai['nama_kapal'] ?></a></td>
												<td><?= $nilai['tanggal_masuk'] ?></td>
												<td><?= $nilai['tanggal_keluar'] ?></td>
												<td><?= $nilai['tahun'] ?></td>
												<td>
													<a href="<?= site_url('Home/delete_kapal/'.$nilai['id_kapal']) ?>" class="btn btn-danger">Delete</a>
													<a href="<?= site_url('Home/edit_kapal/'.$nilai['id_kapal']) ?>" class="btn btn-success">Edit</a>
												</td>
											</tr>
										<?php }?>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="<?php echo base_url().'Form_obat/index'?>" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
              <a href="#" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
</div>

<?php 
 $this->load->view('Admin/footer_admin');
?>
